var xhr= new XMLHttpRequest();
xhr.onreadystatechange=function(){
  if(xhr.readyState===4){
    //console.log(xhr.responseText);
    var employees=JSON.parse(xhr.responseText);
    //console.log(typeof employees);
    var statusHTML='<ul class="bulleted">'
    
    //iterate over the array
    for (let i = 0; i < employees.length; i++) {
      
        //console.dir(employees[i]);
        if(employees[i].inoffice===true){
          statusHTML+='<li class="in">';
        }else{
          statusHTML += '<li class="out">';
        }
        statusHTML+=employees[i].name;
        statusHTML+='</li>';

    }
    //closing ul tag
    statusHTML+='</ul>';
    
    //put the data in employee id tag
    document.getElementById('employeeList').innerHTML=statusHTML;
  }
};
xhr.open('Get','/data/employees.json');
xhr.send();


//which room free or not 
var roomRequest = new XMLHttpRequest();
roomRequest.onreadystatechange = function () {
  if (roomRequest.readyState === 4) {
    //console.log(roomRequest.responseText);
    var rooms = JSON.parse(roomRequest.responseText);
    //console.log(typeof employees);
    var statusHTML = '<ul class="rooms">'

    //iterate over the array
    for (let i = 0; i < rooms.length; i++) {

      //console.dir(employees[i]);
      if (rooms[i].available === true) {
        statusHTML += '<li class="empty">';
      } else {
        statusHTML += '<li class="full">';
      }
      statusHTML += rooms[i].room;
      statusHTML += '</li>';

    }
    //closing ul tag
    statusHTML += '</ul>';

    //put the data in employee id tag
    document.getElementById('roomList').innerHTML = statusHTML;
  }
};
roomRequest.open('Get', '/data/rooms.json');
roomRequest.send();